var height = parseInt(d3.select('body').style('width'),0) * 0.5,
    width = parseInt(d3.select('body').style('width'),0),
    radius = (height / 3) * 0.36,
    long = radius * 1.3,
    temporizador,
    layers = ['background','rect','circle','line'];

var container = d3.select('body')
									.append('svg')
                  .attr('width', width)
                  .attr('height', height);                  

var t =   textures.lines()
    .size(8)
    .strokeWidth(2)
    .stroke('lightblue');

container.call(t);

/* Update pattern layers */
var layers = container.selectAll('g')
											.data(layers)
                      
layers.enter().append('g')

layers.attr('class',function(d) { return d;});

layers.exit().remove();

function redraw(){

height = parseInt(d3.select('body').style('width'),0) * 0.5,
width = parseInt(d3.select('body').style('width'),0),
radius = (height / 3) * 0.36,
long = radius * 1.3,

clearInterval(temporizador);

container.attr('width',width)
					.attr('height',height);

/* Update pattern Background */
var background = d3.select('.background')
										.selectAll('rect')
                    .data(d3.select(this));
                    
background.enter().append('rect');

background.attr('width', width)
					.attr('height', height)
          .style("fill", t.url());

background.exit().remove();

/* Update pattern rect */
var rect = d3.select('.rect')
							.selectAll('rect')
							.data(d3.select(this))
							   
rect.enter().append('rect');
							
rect.attr('class','rect')
		.attr('y', height / 3)
		.attr('width', width)
    .attr('height', height / 3);
    
rect.exit().remove();

/* Update pattern circle */
var circle = d3.select('.circle')
								.selectAll('circle')
                .data(d3.select(this))
								
circle.enter().append('circle');
                
circle.attr('r', radius)
			.attr('cx', width / 2)
    	.attr('cy', height / 2);    	    	
    

circle.exit().remove();

/* Update pattern line */
var gline  = d3.select('.line')
							 .selectAll('g')
               .data(d3.select(this))
                              
gline.enter().append("g")    

gline.attr("transform", "translate(" + width/2 + "," + height/2 + ")");

gline.exit().remove();
              
var line = gline.selectAll("g")
    						.data(d3.range(0, 360, 10))
  							.enter()
  							.append("g")
    						.attr("transform", function(d) { return "rotate(" + -d + ")"; })
               	.append("line")
                
line.attr("x2", long);  

temporizador = setInterval(function(){
							d3.select('circle')                
                .transition()
                .duration(500)
                .attr("r",radius * 1.2)
                .transition()
                .duration(500)
                .attr("r",radius)
                
                d3.selectAll('line')                
                .transition()
                .duration(500)
                .attr("x2",long * 1.2)
                .transition()
                .duration(500)
                .attr("x2",long)
                
                },1000);
}

redraw();
window.addEventListener("resize", redraw);
